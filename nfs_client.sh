#!/bin/bash

# Install NFS client
yum -y install nfs-utils

# Update firewalld to allow only ssh and nfs connections
firewall-cmd --permanent --add-service=ssh
firewall-cmd --permanent --add-rich-rule="rule family='ipv4' source address='10.100.88.251' service name='nfs' accept"
firewall-cmd --permanent --set-default-zone=drop
firewall-cmd --reload

# Mount the network directory
mkdir -p /mnt/upload
# Set permissions for the share directory to allow read and write access
chmod -R 777 /mnt/upload

for i in {1..3}; do
  mount -t nfs 10.100.88.251:/srv/share/upload /mnt/upload && break || sleep 3
  if [ $i -eq 3 ]; then
    echo "Something went wrong." | tee -a errors.log
    journalctl -u nfs-server -n 3 --since "10 minutes ago" | tee -a errors.log
    exit 1
  fi
done

# Create a file in the mounted directory
if ! touch /mnt/upload/client_file; then
  echo "File was not created successfully." | tee -a errors.log
  journalctl -u nfs-server -n 5 --since "10 minutes ago" | tee -a errors.log
  exit 1
else
  echo "File was created successfully."
  # Add the mount to fstab
  echo "10.100.88.251:/srv/share/upload /mnt/upload nfs defaults,vers=3,proto=udp 0 0" >> /etc/fstab
fi