#!/bin/bash

# Install NFS server
yum -y install nfs-utils

# Create the share directory
mkdir -p /srv/share/upload
# Set permissions for the share directory to allow read and write access
chmod -R 777 /srv/share/upload

# Update firewalld to allow only ssh and nfs connections
firewall-cmd --permanent --add-service=ssh
firewall-cmd --permanent --add-rich-rule="rule family='ipv4' source address='10.100.88.252' service name='nfs' accept"
firewall-cmd --permanent --set-default-zone=drop
firewall-cmd --reload

# Export the share directory
echo "/srv/share/upload 10.100.88.252(rw,sync,no_root_squash,no_all_squash,vers=3,proto=udp)" >> /etc/exports

# Start NFS services
systemctl enable rpcbind
systemctl enable nfs-server
systemctl start rpcbind
systemctl start nfs-server

# Catch any error and log to errors.log with related systemd journal data
ERROR=$( { systemctl status nfs-server || systemctl status rpcbind; } 2>&1 )
if [ $? -ne 0 ]; then
  journalctl -u nfs-server >> errors.log
  journalctl -u rpcbind >> errors.log
  echo "$ERROR" >> errors.log
fi